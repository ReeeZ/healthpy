# healthpy

Little Project for parsing data from the Samsung Health App data export feature.

## Usage

Adjust the variables in the `health.py` script so that your data export is referenced. Then run `python health.py`.

## Feature

Currently just plots calorie intake over weight
