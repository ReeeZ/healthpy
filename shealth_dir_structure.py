class SHealthDirStructure():
  def __init__(self, root_directory: str, export_time: str):
    self.root_dir = root_directory
    self.export_time = export_time

  health_prefix = "com.samsung.health."

  def get_weight_file(self) -> str:
    return f"{self.root_dir}/{self.health_prefix}weight.{self.export_time}.csv"

  def get_nutrition_file(self) -> str:
    return f"{self.root_dir}/{self.health_prefix}nutrition.{self.export_time}.csv"

  