import csv
from typing import List
from data import WeightData
from datetime import datetime
from numpy import mean


def mean(numbers: List[float]):
    the_sum = sum(numbers)
    the_len = len(numbers)
    return the_sum / max(the_len, 1)


def _create_weight_data(csv_dict_reader: csv.DictReader) -> List[WeightData]:
    result = []
    for row in csv_dict_reader:
        # There is also update_time and create_time, but in some cases these timestamps were incorrect
        weight_data = WeightData(datetime.strptime(
            row["start_time"], "%Y-%m-%d %H:%M:%S.%f"), float(row["weight"]))
        print(weight_data)

        result.append(weight_data)

    return result


def _process_weight_data(weight_data: List[WeightData]):
    weight_dict = {}
    for w in weight_data:
        day = datetime(w.time.year, w.time.month, w.time.day)
        if day in weight_dict:
            weight_dict[day].append(w.weight)
        else:
            weight_dict[day] = [w.weight]

    print(weight_dict)
    result = []
    for day, weightsOfDay in weight_dict.items():
        print(weightsOfDay)
        result.append(WeightData(day, mean(weightsOfDay)))

    return result


def read_weight_file(weight_csv_file: str) -> List[WeightData]:
    with open(weight_csv_file, "r") as weight_file:
        next(weight_file)  # Skip one line because this first line contains noise
        reader = csv.DictReader(weight_file)
        raw_weight_data = _create_weight_data(reader)
        return _process_weight_data(raw_weight_data)
