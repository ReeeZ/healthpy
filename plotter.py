from data import NutritionData, WeightData
from typing import List
from matplotlib import pyplot as plt


def plot_weight_over_calorie_intake(weight_data: List[WeightData], nutrition_data: List[NutritionData]):
    _, ax = plt.subplots()

    weight_data.sort(key=lambda w: w.time, reverse=True)
    weight_days = [w.time for w in weight_data]
    weight_of_day = [w.weight for w in weight_data]
    weight_lines = ax.plot_date(weight_days, weight_of_day, color="red", marker="o")

    for line in weight_lines:
        line.set_linestyle("-")

    # set x-axis label
    ax.set_xlabel("date", fontsize=14)
    # set y-axis label
    ax.set_ylabel("weight", color="red", fontsize=14)

    # twin object for two different y-axis on the sample plot
    ax2 = ax.twinx()
    
    nutrition_data.sort(key=lambda w: w.time, reverse=True)
    nutrition_days = [n.time for n in nutrition_data]
    total_calories_of_day = [n.calories for n in nutrition_data]
    nutrition_lines = ax2.plot_date(nutrition_days, total_calories_of_day, marker="o")

    for line in nutrition_lines:
        line.set_linestyle("-")

    # make a plot with different y-axis using second axis object
    ax2.plot_date(nutrition_days,
             total_calories_of_day, color="blue", marker="o")
    ax2.set_ylabel("Calories", color="blue", fontsize=14)
    
    plt.show()
