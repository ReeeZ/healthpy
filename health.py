from shealth_dir_structure import SHealthDirStructure
import weight_parser
import nutrition_parser
import plotter

root_dir = "data"
export_time_str = "202201092057"

def main():
    dir_strcuture = SHealthDirStructure(root_dir, export_time_str)
    weight_data = weight_parser.read_weight_file(dir_strcuture.get_weight_file())
    nutrition_data = nutrition_parser.read_nutrition_file(dir_strcuture.get_nutrition_file())
    plotter.plot_weight_over_calorie_intake(weight_data, nutrition_data )
    

if __name__ == "__main__":
    main()
