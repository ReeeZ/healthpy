import csv
from typing import List
from data import NutritionData
from datetime import datetime


def _create_nutrition_data(csv_dict_reader: csv.DictReader) -> List[NutritionData]:
    result = []
    for row in csv_dict_reader:
        # There is also update_time and create_time, but in some cases these timestamps were incorrect
        nutrition_data = NutritionData(datetime.strptime(
            row["start_time"], "%Y-%m-%d %H:%M:%S.%f"), float(row["calorie"]))
        print(nutrition_data)

        result.append(nutrition_data)

    return result


def _process_nutrition_data(nutrition_data: List[NutritionData]):
    nutrition_dict = {}
    for n in nutrition_data:
        day = datetime(n.time.year, n.time.month, n.time.day)
        if day in nutrition_dict:
            nutrition_dict[day].append(n.calories)
        else:
            nutrition_dict[day] = [n.calories]

    print(nutrition_dict)
    result = []
    for day, food_of_day in nutrition_dict.items():
        print(food_of_day)
        result.append(NutritionData(day, sum(food_of_day)))

    return result


def read_nutrition_file(nutrition_csv_file: str) -> List[NutritionData]:
    with open(nutrition_csv_file, "r") as nutrition_file:
        # Skip one line because this first line contains noise
        next(nutrition_file)
        reader = csv.DictReader(nutrition_file)
        raw_data = _create_nutrition_data(reader)
        return _process_nutrition_data(raw_data)
