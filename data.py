from dataclasses import dataclass
from datetime import datetime
from typing import Dict


@dataclass
class WeightData():
    time: datetime
    weight: float

@dataclass
class NutritionData():  
    time: datetime
    calories: float
